import java.util.Scanner;

public class App02 {

	public static void main(String[] args) {
		//Inicializa��o de vari�veis
		int a = 0, b = 0;
		double divisao = 0;
		
		Scanner tecla = new Scanner(System.in);
		
		//Entrada dos dados
		System.out.println("Digite um valor inteiro:");
		a = tecla.nextInt();
		
		//Estrutura de repeti��o
		do {
			System.out.println("Digite outro valor inteiro: ");
			b = tecla.nextInt();
		}while(b == 0);
		
		//Processamento da informa��o
		divisao = a / b;
		System.out.println("A divisao do primeiro valor pelo segundo �: " + divisao);
	}

}
